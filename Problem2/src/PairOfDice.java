public class PairOfDice
{
  Die die1 = new Die();
  Die die2 = new Die();
  public PairOfDice()
  {
    rollDice();
  }
  
  public void rollDice()
  {
    die1.roll();
    die2.roll();
  }
  public int sumDice()
  {
    return die1.getFaceValue() + die2.getFaceValue();
  }
  
  public void setDie1(int set)
  {
    die1.setFaceValue(set);
  }
  public int getDie1()
  {
    return die1.getFaceValue();
  }
  
  public void setDie2(int set)
  {
    die2.setFaceValue(set);
  }
  public int getDie2()
  {
    return die2.getFaceValue();
  }
}
