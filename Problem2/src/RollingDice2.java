//********************************************************************
//File:         RollingDice2.java       
//Author:       Kalvin Dewey
//Date:         10/16/17
//Course:       CPS100
//
//Problem Statement:
//Using the Die class defined in this chapter, write a class called
//PairOfDice, composed of two Die objects. Include methods to
//set and get the individual die values, a method to roll the dice,
//and a method that returns the current sum of the two die values.
//Create a driver class called RollingDice2 to instantiate and use a
//PairOfDice object.
//
//Inputs:   None
//Outputs:  tests of class PairOfDice
// 
//********************************************************************
public class RollingDice2
{

  public static void main(String[] args)
  {
    PairOfDice dice = new PairOfDice();
    System.out.println("Die1: " + dice.getDie1());
    System.out.println("Die2: " + dice.getDie2());
    System.out.println("Sum: " + dice.sumDice());
    
    dice.rollDice();
    System.out.println("Die1: " + dice.getDie1());
    System.out.println("Die2: " + dice.getDie2());
    System.out.println("Sum: " + dice.sumDice());
    
    dice.setDie1(4);
    System.out.println("Die1: " + dice.getDie1());
    dice.setDie2(2);
    System.out.println("Die2: " + dice.getDie2());
    
  }

}
