//********************************************************************
//File:         Bookshelf.java       
//Author:       Kalvin Dewey
//Date:         10/15/17
//Course:       CPS100
//
//Problem Statement:
//Write a class called Book that contains instance data for the title,
//author, publisher, and copyright date. Define the Book construc-
//tor to accept and initialize this data. Include setter and getter
//methods for all instance data. Include a toString method that
//returns a nicely formatted, multi-line description of the book.
//Create a driver class called Bookshelf, whose main method
//instantiates and updates several Book objects.
//
//Inputs:   None
//Outputs:  tests of class Book
// 
//********************************************************************
public class Bookshelf
{

  public static void main(String[] args)
  {
    Book book1 = new Book("Empire From The Ashes", "tba", "Baen Books", 2003);
    Book book2 = new Book("title", "Robert L. Forward", "publisher", 2050);

    //test all setters and getters
    System.out.println(book2.getTitle());
    book2.setTitle("Dragons's Egg");
    System.out.println(book2.getTitle());

    System.out.println(book1.getAuthor());
    book1.setAuthor("David Weber");
    System.out.println(book1.getAuthor());

    System.out.println(book2.getPublisher());
    book2.setPublisher("Del Ray");
    System.out.println(book2.getPublisher());

    System.out.println(book2.getCopyright());
    book2.setCopyright(1980);
    System.out.println(book2.getCopyright());
    //print out finished Books
    System.out.println();
    System.out.println(book1);
    System.out.println();
    System.out.println(book2);
  }

}
