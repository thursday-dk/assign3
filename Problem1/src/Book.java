
public class Book
{
  private String title;
  private String author;
  private String publisher;
  private int copyright;
  
  public Book(String strTitle, String strAuthor, String strPublish, int intCopy)
  {
    title = strTitle;
    author = strAuthor;
    publisher = strPublish;
    copyright = intCopy;
  }
  
  public String toString()
  {
    String working = "";
    working += "Title: " + title + "\nAuthor: " + author;
    return working + "\nPublisher: " + publisher + "\nCopyright: " + copyright;
  }

  public String getTitle()
  {
    return title;
  }
  public void setTitle(String strTitle)
  {
    title = strTitle;
  }
  
  public String getAuthor()
  {
    return author;
  }

  public void setAuthor(String strAuthor)
  {
    author = strAuthor;
  }

  public String getPublisher()
  {
    return publisher;
  }

  public void setPublisher(String strPublisher)
  {
    publisher = strPublisher;
  }

  public int getCopyright()
  {
    return copyright;
  }

  public void setCopyright(int intCopyright)
  {
    copyright = intCopyright;
  }
}
